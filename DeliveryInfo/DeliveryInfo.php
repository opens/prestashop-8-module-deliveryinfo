<?php

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

if (!defined('_PS_VERSION_')) {
    exit;
}

class DeliveryInfo extends Module implements WidgetInterface
{
    public function __construct()
    {
        $this->name = 'DeliveryInfo';
        $this->tab = 'delivery_info';
        $this->author = 'Opens';
        $this->version = '1.0.0';
        parent::__construct();

        $this->displayName = $this->trans('Cena dostawy', [], 'Modules.Sharebuttons.Admin');
        $this->description = $this->trans('Wyświetl najniższą cenę dostawy na stronie produktu dla pojedynczego produktu', [], 'Modules.Sharebuttons.Admin');
    }

    public function install()
    {
        return parent::install()
            && $this->registerHook('displayProductAdditionalInfo');
    }

    public function renderWidget($hookName, array $params)
    {
        $context = Context::getContext();
        $cart = new Cart();
        $cart->id = 1;
        $cart->id_currency = $context->currency->id;
        $cart->id_lang = $context->language->id;
        $products = $cart->getProducts();
        foreach ($products as $product) {
            $cart->deleteProduct($product['id_product'], $product['id_product_attribute']);
        }
        $cart->save();
        $cart->updateQty(1, $params['product']['id']);
        $cart->update();

        $priceWithTax = [];

        foreach ($cart->getDeliveryOptionList($context->country) as $v) {
            foreach ($v as $v2) {
                foreach ($v2['carrier_list'] as $id => $v3) {
                    if ($v3['price_with_tax'] > 0)
                        $priceWithTax[$id] = $v3['price_with_tax'];
                }
            }
        }
        if (count($priceWithTax) > 0)
            return $this->trans('Delivery', [], 'Admin.Global') . " " . $this->trans('from', [], 'Admin.Global') . ': ' . min($priceWithTax) . " " . (new Currency($cart->id_currency))->getSymbol();
        return null;
    }

    public function getWidgetVariables($hookName, array $configuration)
    {
        return null;
    }
}
